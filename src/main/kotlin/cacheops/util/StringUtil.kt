package cacheops.util

fun formatObjNameToFileName(name: String, id: Int): String {
    val nameFormatted = name.replace(" ", "_", true)
        .replace("_(", "_", true).replace(")_", "_", true)
        .replace("(", "_", true).replace(")", "", true)
        .replace("+", "P", true).replace("P+", "PP", true)
        .replace("'", "", true).replace("-", "_", true).uppercase()
    val namePass2 = nameFormatted.replace("G__", "G_", true)
        .replace("E__", "E_", true).replace("T__", "T_", true)
        .replace("K__", "K_", true).replace("P__", "P_", true)
        .replace("R__", "R_", true).replace("B__", "B_", true)
        .replace("D__", "D_", true)
    return "OBJ${id}_${namePass2}.obj"
}

fun formatEnumNameToFileName(name: String, id: Int): String {
    return "ENUM${id}_${name}.enum"
}