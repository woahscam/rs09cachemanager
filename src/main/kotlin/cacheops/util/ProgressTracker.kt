package cacheops.util

import com.displee.cache.ProgressListener
import logger.Log
import logger.log

class ProgressTracker: ProgressListener {
    override fun notify(progress: Double, message: String?) {
        log(this::class.java, Log.FINE, "$message - [${(progress * 100).toInt()}%]")
    }
}