package cacheops.util

object Archives {
    /**
     * Archives that are located in the CONFIGURATION index (2)
     */
    const val FLOOR_UNDERLAYS = 1
    const val ARCHIVE_2 = 2             // 270 files, SIZE 1, DATA byte 0
    const val PLAYER_KIT = 3
    const val FLOOR_OVERLAYS = 4
    const val INVENTORIES = 5
    const val ARCHIVE_7 = 7             // 297 files, SIZE 1, DATA byte 0
    const val PARAM_TYPE = 11
    const val ARCHIVE_15 = 15           // 57 files, SIZE 1, DATA byte 0
    const val VAR_PLAYER = 16
    const val ARCHIVE_18 = 18           // 530 : 553 files, SIZE 1, DATA byte 0
    const val VAR_CLIENT = 19           // 530 : 631 files, SIZE 1, DATA byte 0 || 578 : 1048 files, SIZE variable, DATA variable
    const val ARCHIVE_20 = 20           // 530 : 22 files, SIZE 1, DATA byte 0
    const val ARCHIVE_21 = 21           // 530 : 8 files, SIZE 1, DATA byte 0
    const val ARCHIVE_22 = 22           // 530 : 842 files, SIZE 1, DATA byte 0
    const val ARCHIVE_23 = 23           // 530 : 229 files, SIZE 1, DATA byte 0
    const val ARCHIVE_24 = 24           // 530 : 11 files, SIZE 1, DATA byte 0
    const val ARCHIVE_25 = 25           // 530 : 128 files, SIZE 1, DATA byte 0
    const val STRUCT_TYPE = 26          // 530 : N/A || 578 :
    const val LIGHT_TYPE = 31
    const val BAS_ANIMATION = 32
    const val CURSOR = 33
    const val MAP_SCENE_TYPE = 34
    const val QUESTS = 35

    /**
     *
     * All of the archives located in the WORLD_MAP index (23)
     * [MAIN] = Archive 0
     * [DETAILS] = Archive 1
     * [LABELS] = Archive 2
     */
    const val MAIN = "main"
    const val DETAILS = "details"
    const val LABELS = "main_labels"

    /**
     * Archives that are located in the QUICKCHAT_MESSAGES index (24)
     */
    const val CATEGORIES = 0
    const val PHRASES = 1

    /**
     * Archives that are located in the QUICKCHAT_MENUS index (25)
     * These are possibly used for tests, File 0 for each respected
     * archive returns:
     * "Test cat" (Test category)
     * "Mike's test chatphrase" (Test phrases)
     * These are duplicates of what is already inside index 24.
     */
    const val CATEGORY_TESTS = 0
    const val PHRASES_TESTS = 1

}