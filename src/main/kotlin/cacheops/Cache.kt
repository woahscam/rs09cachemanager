package cacheops

import cacheops.types.cenum.EnumTypeDataHandler
import cacheops.types.cenum.EnumTypeDefinition
import cacheops.types.obj.ObjectDataHandler
import cacheops.types.obj.ObjectDefinition
import cacheops.types.obj.ObjectUtil
import cacheops.util.ProgressTracker
import com.displee.cache.CacheLibrary
import com.displee.cache.index.archive.file.File
import java.io.FileNotFoundException


// Supports only revisions 4xx - 530 (after they removed npc, obj, scenery configs from the config index (2))
enum class Cache(val id: Int) : LookupLocation {
//    FRAMES(0),
//    FRAME_MAPS(1),
//    CONFIGURATION(2),
//    INTERFACES(3),
//    SYNTH_SOUNDS(4),
//    LANDSCAPES(5),
//    MUSIC(6),
//    MESHES(7),
//    SPRITES(8),
//    TEXTURES(9),
//    HUFFMAN_ENCODING(10),
//    MIDI_JINGLES(11),
//    CLIENT_SCRIPTS(12),
//    FONT_METRICS(13),
//    VORBIS(14),
//    MIDI_INSTRUMENTS(15),
//    CONFIGURATION_SCENERY(16),
    CONFIGURATION_ENUM(17) {
    override fun getDef(id: Int): Definition? {
        val def = EnumTypeDefinition(id = id)
        library.index(this.id).archive(def.archiveId())?.file(def.fileId())?.data?.let {
            return EnumTypeDataHandler().decode(def, it)
        }
        return null
    }

    override fun getFileAmount(): Int {
        val lastArchiveId = library.index(this.id).last()!!.id
        return lastArchiveId * 256 + library.index(this.id).last()!!.files().size
    }

    override fun packLooseFromDir() {
        val map = EnumTypeDataHandler().tomlToDefinition("flatfile/17_ENUM_CONFIGURATION/").toSortedMap()
        val highest = map.lastKey()
        for (n in 0..highest) {
            val data = if (map.contains(n)) {
                EnumTypeDataHandler().encode(def = map.getValue(n))
            } else {
                byteArrayOf(0)
            }

            val file = File(id = (n and 0xff), data = data)
            library.index(this.id).archive(n ushr 8)!!.add(file = file, overwrite = true)
        }

        library.update()
    }
},
//    CONFIGURATION_NPC(18),
    CONFIGURATION_OBJECT(19) {
        override fun getDef(id: Int): Definition? {
            val def = ObjectDefinition(id = id)
            library.index(this.id).archive(def.archiveId())?.file(def.fileId())?.data?.let {
                println(it.size)
                return ObjectDataHandler().decode(def, it)
            }
            return null
        }

    override fun getFileAmount(): Int {
        val lastArchiveId = library.index(this.id).last()!!.id
        return lastArchiveId * 256 + library.index(this.id).last()!!.files().size
    }

    override fun packLooseFromDir() {
        println(ObjectDataHandler().encode(ObjectUtil.curObj!!))
        //"flatfile/19_OBJECT_CONFIGURATION/"
    }
};
//    CONFIGURATION_SEQUENCES(20),
//    CONFIGURATION_SPOTANIM(21),
//    CONFIGURATION_VARBIT(22),
//    WORLD_MAP(23),
//    QUICKCHAT_MESSAGES(24),
//    QUICKCHAT_MENUS(25),
//    MATERIALS(26),
//    CONFIGURATION_PARTICLES(27);

    companion object {
        lateinit var library: CacheLibrary

        fun init(location: String): Boolean {
            // Plopped here due to Unit Test, sneaky way to remove of null
            if (this::library.isInitialized) {
                return true
            }

            // If the Cache library could not be initialized, it throws an exception
            // Should not continue if false
            try {
                library = CacheLibrary.create(path = location, listener = ProgressTracker())
            } catch (noCache: FileNotFoundException) {
                return false
            }
            return true
        }
    }
}