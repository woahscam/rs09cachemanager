package cacheops

import cacheops.types.cenum.EnumEntryNames
import cacheops.types.cenum.EnumTypeDefinition
import cacheops.types.obj.ObjectDefinition
import cacheops.types.obj.ObjectUtil
import cacheops.util.formatEnumNameToFileName
import cacheops.util.formatObjNameToFileName
import com.moandjiezana.toml.TomlWriter
import java.io.File


interface LookupLocation {
    fun getDef(id: Int): Definition?

    fun getFileAmount(): Int

    fun loopDefToTOML() {
        for (n in 0..getFileAmount()) {
            toml(n)
        }
    }

    fun packLooseFromDir()

    private fun toml(id: Int) {
        val def = getDef(id)
        if (def != null) {
            val map = def.tomlFormat()

            if (map.isNotEmpty()) {
                val tomlWriter = TomlWriter()
                val tomlString: String = tomlWriter.write(map)

                when (def) {
                    is ObjectDefinition -> {
                        val name = ObjectUtil.nameOverride.getOrDefault(id, "null")
                        val formatted = "[${
                            name
                                .lowercase()
                                .replace(" ", "_")
                        }]\n" + tomlString

                        val fileName = formatObjNameToFileName(name, id)
                        File("flatfile/19_OBJECT_CONFIGURATION/$fileName").printWriter().use { out ->
                            out.println(formatted)
                        }
                    }

                    is EnumTypeDefinition -> {
                        if (tomlString.isNotBlank()) {
                            val name = (EnumEntryNames from id)?.name ?: "UNKNOWN"
                            val formatted = "[${name.lowercase()}]\n" + tomlString
                            val fileName = formatEnumNameToFileName(name = name, id = id)
                            File("flatfile/17_ENUM_CONFIGURATION/$fileName").printWriter().use { out ->
                                out.println(formatted)
                            }
                        }
                    }
                }
            }
        }
    }
}