package cacheops.types.obj

import cacheops.types.DataHandler
import com.displee.io.impl.InputBuffer
import com.displee.io.impl.OutputBuffer
import java.io.File

class ObjectDataHandler: DataHandler<ObjectDefinition> {

    override fun decode(def: ObjectDefinition, data: ByteArray): ObjectDefinition {
        val buffer = InputBuffer(data)
        while (true) {
            val opcode: Int = buffer.readUnsignedByte()
            println("OPCODE [$opcode]")
            if (opcode == 0) {
                return def
            }
            var count: Int
            when (opcode) {
                // SETTINGS (CORE)
                2 -> def.name = buffer.readString()
                12 -> def.cost = buffer.readInt()
                16 -> def.members = true
                65 -> def.stockMarket = true
                115 -> def.team = buffer.readUnsignedByte()
                96 -> def.dummyItem = buffer.readUnsignedByte()
                in 30..39 -> { // Contains both ops + iops
                    val op: String = buffer.readString()
                    if (opcode <= 34) { // Ground operations. We can deal with hidden here.
//                        if (op.contentEquals(other = "Hidden", ignoreCase = true)) {
//                            op = null
//                        }
                        def.ops[opcode - 30] = op
                    } else { // Interface operations
                        def.iops[opcode - 35] = op
                    }
                }
                249 -> {
                    count = buffer.readUnsignedByte()
                    val param: HashMap<Long, Any> = HashMap(count)
                    for (n in 0..<count) {
                        val value: Any
                        val isString = buffer.readUnsignedByte() == 1
                        val key = buffer.read24BitInt()
                        param[key.toLong()] = if (isString) buffer.readString() else buffer.readInt()
                    }
                    def.params = param
                }

                // STACKABLE
                11 -> def.stackable = true
                in 100..109 -> {
                    def.countObj[opcode - 100] = buffer.readUnsignedShort()
                    def.countCo[opcode - 100] = buffer.readUnsignedShort()
                }

                // NOTE/LENDING
                97 -> def.certlink = buffer.readUnsignedShort()
                98 -> def.certtemplate = buffer.readUnsignedShort()
                121 -> def.lentLink = buffer.readUnsignedShort()
                122 -> def.lentTemplate = buffer.readUnsignedShort()

                // INTERFACE SPRITE
                4 -> def.zoom2D = buffer.readUnsignedShort()
                5 -> def.xAngle2D = buffer.readUnsignedShort()
                6 -> def.yAngle2D = buffer.readUnsignedShort()
                7 -> {
                    var xOff = buffer.readUnsignedShort()
                    if (xOff > 32767) {
                        xOff -= 65536
                    }
                    def.xOffset2D = xOff
                }
                8 -> {
                    var yOff = buffer.readUnsignedShort()
                    if (yOff > 32767) {
                        yOff -= 65536
                    }
                    def.yOffset2D = yOff
                }
                95 -> def.zAngle2D = buffer.readUnsignedShort()
                110 -> def.resizeX = buffer.readUnsignedShort()
                111 -> def.resizeY = buffer.readUnsignedShort()
                112 -> def.resizeZ = buffer.readUnsignedShort()


                // MESHES
                1 -> def.model = buffer.readUnsignedShort()
                // MEQ
                23 -> def.maleEquip1 = buffer.readUnsignedShort()
                24 -> def.maleEquip2 = buffer.readUnsignedShort()
                78 -> def.maleEquip3 = buffer.readUnsignedShort()
                90 -> def.primaryMaleDialogueHead = buffer.readUnsignedShort()
                92 -> def.secondaryMaleDialogueHead = buffer.readUnsignedShort()
                // OFFSETS
                125 -> {
                    def.maleWieldX = buffer.readByte().toInt()
                    def.maleWieldY = buffer.readByte().toInt()
                    def.maleWieldZ = buffer.readByte().toInt()
                }

                // FEQ
                25 -> def.femaleEquip1 = buffer.readUnsignedShort()
                26 -> def.femaleEquip2 = buffer.readUnsignedShort()
                79 -> def.femaleEquip3 = buffer.readUnsignedShort()
                91 -> def.primaryFemaleDialogueHead = buffer.readUnsignedShort()
                93 -> def.secondaryFemaleDialogueHead = buffer.readUnsignedShort()
                // OFFSETS
                126 -> {
                    def.femaleWieldX = buffer.readByte().toInt()
                    def.femaleWieldY = buffer.readByte().toInt()
                    def.femaleWieldZ = buffer.readByte().toInt()
                }

                // COLOR SETTINGS
                113 -> def.ambient = buffer.readByte().toInt()
                114 -> def.contrast = (buffer.readByte().toInt()) * 5
                40, 41 -> {
                    count = buffer.readUnsignedByte()
                    val map = LinkedHashMap<Int, Int>()
                    for (n in 0..<count) {
                        map[buffer.readUnsignedShort()] = buffer.readUnsignedShort()
                    }

                    if (opcode == 40) { // Recoloring
                        def.reColor = map
                    } else {            // Retexturing
                        def.reTexture = map
                    }
                }
                42 -> {
                    count = buffer.readUnsignedByte()
                    val palette = ByteArray(count)
                    for (n in 0..<count) {
                        palette[n] = buffer.readByte()
                    }
                    def.recolourPalette = palette
                }

                // IMPLEMENTED IN LATER REVISIONS
                127, 128 -> {
                    val cursorOp = buffer.readUnsignedByte()
                    val cursor = buffer.readUnsignedShort()
                    if (opcode == 127) {
                        def.primaryCursorOpcode = cursorOp
                        def.primaryCursor = cursor
                    } else {
                        def.secondaryCursorOpcode = cursorOp
                        def.secondaryCursor = cursor
                    }
                }
                129, 130 -> {
                    val iCursorOP = buffer.readUnsignedByte()
                    val iCursor = buffer.readUnsignedShort()
                    if (opcode == 129) {
                        def.primaryInterfaceCursorOpcode = iCursorOP
                        def.primaryInterfaceCursor = iCursor
                    } else {
                        def.secondaryInterfaceCursorOpcode = iCursorOP
                        def.secondaryInterfaceCursor = iCursor
                    }
                }
                else -> println("Unhandled opcode $opcode, are you using a different revision cache?")
            }
        }
    }

    override fun encode(def: ObjectDefinition): ByteArray {
        val size = determineBufferSize(def)
        val buffer = OutputBuffer(size)

        return buffer.raw()
    }

    override fun determineBufferSize(def: ObjectDefinition): Int {
        var bufSize = 0

        if (!def.name.contentEquals(other = "null", ignoreCase = true)) {
            bufSize++
            bufSize += (def.name.toByteArray().size + 1)
        }
        if (def.cost != 1) bufSize += 5
        if (def.members) bufSize++
        if (def.stockMarket) bufSize++
        if (def.team != 0) bufSize += 2
        if (def.dummyItem != 0) bufSize += 2
        def.ops.forEach { operation ->
            if (!operation.isNullOrBlank()) {
                if (!operation.contentEquals(other = "Take", ignoreCase = true)) {
                    bufSize++
                    bufSize += (operation.toByteArray().size + 1)
                }
            }
        }

        def.iops.forEach { iOperation ->
            if (!iOperation.isNullOrBlank()) {
                if (!iOperation.contentEquals(other = "Drop", ignoreCase = true)) {
                    bufSize++
                    bufSize += (iOperation.toByteArray().size + 1)
                }
            }
        }

        if (def.params != null) {
            bufSize += 2
            for (n in def.params!!) {
                bufSize += 4
                when (val type = n.value) {
                    is Int -> bufSize += 4
                    is String -> bufSize += (type.toByteArray().size + 1)
                }
            }
        }

        if (def.stackable) bufSize++

        def.countCo.forEach {
            if (it != null) {
                bufSize++
                bufSize += 4
            }
        }

        if (def.certlink != -1) bufSize += 3
        if (def.certtemplate != -1) bufSize += 3
        if (def.lentLink != -1) bufSize += 3
        if (def.lentTemplate != -1) bufSize += 3

        if (def.zoom2D != 2000) bufSize += 3
        if (def.xOffset2D != 0) bufSize += 3
        if (def.yOffset2D != 0) bufSize += 3
        if (def.xAngle2D != 0) bufSize += 3
        if (def.yAngle2D != 0) bufSize += 3
        if (def.zAngle2D != 0) bufSize += 3
        if (def.resizeX != 128) bufSize += 3
        if (def.resizeY != 128) bufSize += 3
        if (def.resizeZ != 128) bufSize += 3

        if (def.model != -1) bufSize += 3
        if (def.maleEquip1 != -1) bufSize += 3
        if (def.maleEquip2 != -1) bufSize += 3
        if (def.maleEquip3 != -1) bufSize += 3
        if (def.primaryMaleDialogueHead != -1) bufSize += 3
        if (def.secondaryMaleDialogueHead != -1) bufSize += 3
        if (def.maleWieldX != 0 || def.maleWieldY != 0 || def.maleWieldZ != 0) {
            bufSize += 4
        }
        if (def.femaleEquip1 != -1) bufSize += 3
        if (def.femaleEquip2 != -1) bufSize += 3
        if (def.femaleEquip3 != -1) bufSize += 3
        if (def.primaryFemaleDialogueHead != -1) bufSize += 3
        if (def.secondaryFemaleDialogueHead != -1) bufSize += 3
        if (def.femaleWieldX != 0 || def.femaleWieldY != 0 || def.femaleWieldZ != 0) {
            bufSize += 4
        }

        if (def.ambient != 0) bufSize += 2
        if (def.contrast != 0) bufSize += 2

        if (def.reColor != null) {
            bufSize += 2
            for (n in def.reColor!!) {
                bufSize += 4
            }
        }

        if (def.reTexture != null) {
            bufSize += 2
            for (n in def.reTexture!!) {
                bufSize += 4
            }
        }

        if (def.recolourPalette != null) {
            bufSize += 2
            for (n in def.recolourPalette!!) {
                bufSize++
            }
        }

        bufSize++

        return bufSize
    }

    override fun tomlToDefinition(path: String): HashMap<Int, ObjectDefinition> {
        val encodeMap: HashMap<Int, ObjectDefinition> = HashMap()
        val file = File(path)
        file.listFiles()?.forEach { defFile ->
            val def = decodeTOML(defFile)
            if (def != null) {
                encodeMap[def.id] = def
            }
        }
        return encodeMap
    }

    override fun decodeTOML(file: File): ObjectDefinition? {
        TODO("Not yet implemented")
    }
}