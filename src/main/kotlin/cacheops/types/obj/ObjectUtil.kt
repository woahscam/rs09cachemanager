package cacheops.types.obj

object ObjectUtil {

    var nameOverride: HashMap<Int, String> = HashMap()

    var curObj: ObjectDefinition? = null
    /**
     * Rev. 530 CS2 script that uses OP4208 (get obj attribute)
     * 1503
     * 1864
     *
     * Object parameter enum
     *
     * @property id
     * @constructor Create empty Item param enum
     */
    enum class ItemParamEnum(val id: Int) {
        // Used in 530
        DESTROY_ITEM(59),
        IFACE_REQUIRED_OBJECT_0(211),
        IFACE_REQUIRED_OBJECT_0_AMOUNT(212),
        IFACE_REQUIRED_OBJECT_1(213),
        IFACE_REQUIRED_OBJECT_1_AMOUNT(214),
        IFACE_REQUIRED_OBJECT_2(215),
        IFACE_REQUIRED_OBJECT_2_AMOUNT(216),
        IFACE_REQUIRED_OBJECT_3(217),
        IFACE_REQUIRED_OBJECT_3_AMOUNT(218),
        IFACE_REQUIRED_OBJECT_4(219),
        IFACE_REQUIRED_OBJECT_4_AMOUNT(220),
        IFACE_REQUIRED_OBJECT_5(221),
        IFACE_REQUIRED_OBJECT_5_AMOUNT(222),

        // Added in later revs
        UNTRIMMED_SKILLCAPE(258),
        TRIMMED_SKILLCAPE(259),
        SKILL_ID(277),
        RIGHT_CLICK_OP_0(528),
        RIGHT_CLICK_OP_1(529),
        RIGHT_CLICK_OP_2(530),
        RIGHT_CLICK_OP_3(531),
        WEAPON_TYPE(686),
        HAS_SPEC_ATTACK(687),
        QUEST_REQUIREMENT(743),
        //QUEST_REQUIREMENT(891),
        RENDER_ANIMATION(644),
        REQ_SKILL_ID_0(749),
        REQ_SKILL_LEVEL_0(750),
        REQ_SKILL_ID_1(751),
        REQ_SKILL_LEVEL_1(752),
        REQ_SKILL_ID_2(753),
        REQ_SKILL_LEVEL_2(754),
        REQ_SKILL_ID_3(755),
        REQ_SKILL_LEVEL_3(756),
        REQ_SKILL_ID_4(757),
        REQ_SKILL_LEVEL_4(758),
        MOB_ARMIES_RC_COST(821)
    }

}
/* CS2 ref script 1503 - object parameters

void script_1503(int arg0) {
	int int1;
	Item item2;
	int1 = 0;
	item2 = null;
	if (getClientCycle() > arg0) {
		widget(763, 0).hookFrame(null);
		while (CHILD.setChild(widget(763, 0), int1) == true) {
			item2 = CHILD.getItemId();
			if (item2 != null) {
				if (itemAttribute(item2, 59) != 1) {
					CHILD.setTrans(200);
					script_1735(widget(763, 0), int1);
				}
				int1 = 100;
			}
			int1 = int1 + 1;
		}
	}
	return;
}
 */

/* CS2 ref script 1864 - object parameters

void script_1864(int arg0, Item item1, int arg2, int arg3) {
	int int4;
	if (arg0 != 1) {
		return;
	}
	switch (arg2) {
		case 12:
			switch (arg3) {
				case 0:
					msg("Rick: " + datamap('i', 'i', 238, 1) + " coins.");
					break;
				case 1:
					msg("Maid: " + datamap('i', 'i', 238, 3) + " coins.");
					break;
				case 2:
					msg("Cook: " + datamap('i', 'i', 238, 5) + " coins.");
					break;
				case 3:
					msg("Butler: " + datamap('i', 'i', 238, 6) + " coins.");
					break;
				case 4:
					msg("Demon Butler: " + datamap('i', 'i', 238, 8) + " coins.");
					break;
			}
			return;
		case 0:
			switch (item1) {
				case 8170:
					item1 = 8415;
					break;
				case 8315:
					item1 = 8395;
					break;
				case 8222:
					item1 = 8396;
					break;
				case 8121:
					item1 = 8397;
					break;
				case 8379:
					item1 = 8406;
					break;
				case 8037:
					item1 = 8398;
					break;
				case 8255:
					item1 = 8401;
					break;
				case 8163:
					item1 = 8399;
					break;
				case 8027:
					item1 = 8400;
					break;
				case 8259:
					item1 = 8403;
					break;
				case 8340:
					item1 = 8407;
					break;
				case 9822:
					item1 = 9842;
					break;
				case 8068:
					item1 = 8405;
					break;
				case 8333:
					item1 = 8408;
					break;
				case 8195:
					item1 = 8416;
					break;
				case 8363:
					item1 = 8409;
					break;
				case 8302:
					item1 = 8410;
					break;
				case 8123:
					item1 = 8411;
					break;
				case 8151:
					item1 = 8414;
					break;
			}
			msg(getItemName(item1) + ": " + getItemCost(item1) + " coins.");
			return;
		case 13:
		case 14:
			return;
	}
	ARRAY0 = new Item[6];
	ARRAY1 = new int[6];
	int4 = 0;
	if (itemAttribute(item1, 211) != null) {
		ARRAY0[0] = itemAttribute(item1, 211);
		ARRAY1[0] = itemAttribute(item1, 212);
		int4 = int4 + 1;
	}
	if (itemAttribute(item1, 213) != null) {
		ARRAY0[1] = itemAttribute(item1, 213);
		ARRAY1[1] = itemAttribute(item1, 214);
		int4 = int4 + 1;
	}
	if (itemAttribute(item1, 215) != null) {
		ARRAY0[2] = itemAttribute(item1, 215);
		ARRAY1[2] = itemAttribute(item1, 216);
		int4 = int4 + 1;
	}
	if (itemAttribute(item1, 217) != null) {
		ARRAY0[3] = itemAttribute(item1, 217);
		ARRAY1[3] = itemAttribute(item1, 218);
		int4 = int4 + 1;
	}
	if (itemAttribute(item1, 219) != null) {
		ARRAY0[4] = itemAttribute(item1, 219);
		ARRAY1[4] = itemAttribute(item1, 220);
		int4 = int4 + 1;
	}
	if (itemAttribute(item1, 221) != null) {
		ARRAY0[5] = itemAttribute(item1, 221);
		ARRAY1[5] = itemAttribute(item1, 222);
		int4 = int4 + 1;
	}
	switch (int4) {
		case 1:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + ".");
			break;
		case 2:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + " and " + ARRAY1[1] + " x " + toLowerCase(getItemName((Item)ARRAY0[1])) + ".");
			break;
		case 3:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + ", " + ARRAY1[1] + " x " + toLowerCase(getItemName((Item)ARRAY0[1])) + " and " + ARRAY1[2] + " x " + toLowerCase(getItemName((Item)ARRAY0[2])) + ".");
			break;
		case 4:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + ", " + ARRAY1[1] + " x " + toLowerCase(getItemName((Item)ARRAY0[1])) + ",");
			msg(ARRAY1[2] + " x " + toLowerCase(getItemName((Item)ARRAY0[2])) + " and " + ARRAY1[3] + " x " + toLowerCase(getItemName((Item)ARRAY0[3])) + ".");
			break;
		case 5:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + ", " + ARRAY1[1] + " x " + toLowerCase(getItemName((Item)ARRAY0[1])) + ",");
			msg(ARRAY1[2] + " x " + toLowerCase(getItemName((Item)ARRAY0[2])) + ", " + ARRAY1[3] + " x " + toLowerCase(getItemName((Item)ARRAY0[3])) + " and " + ARRAY1[4] + " x " + toLowerCase(getItemName((Item)ARRAY0[4])) + ".");
			break;
		case 6:
			msg(getItemName(item1) + ": " + ARRAY1[0] + " x " + toLowerCase(getItemName((Item)ARRAY0[0])) + ", " + ARRAY1[1] + " x " + toLowerCase(getItemName((Item)ARRAY0[1])) + ", " + ARRAY1[2] + " x " + toLowerCase(getItemName((Item)ARRAY0[2])) + ",");
			msg(ARRAY1[3] + " x " + toLowerCase(getItemName((Item)ARRAY0[3])) + ", " + ARRAY1[4] + " x " + toLowerCase(getItemName((Item)ARRAY0[4])) + " and " + ARRAY1[5] + " x " + toLowerCase(getItemName((Item)ARRAY0[5])) + ".");
			break;
		default:
			msg(getItemName(item1));
			break;
	}
	return;
}
 */