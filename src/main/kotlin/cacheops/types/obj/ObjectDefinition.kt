package cacheops.types.obj

import cacheops.Definition

data class ObjectDefinition(
    override var id: Int = -1,

    // SETTINGS (CORE)
    var name: String = "null",
    var cost: Int = 1,
    var members: Boolean = false,
    var stockMarket: Boolean = false,
    var team: Int = 0,
    var dummyItem: Int = 0,
    var ops: Array<String?> = arrayOf(null, null, "Take", null, null),
    var iops: Array<String?> = arrayOf(null, null, null, null, "Drop"),
    var params: HashMap<Long, Any>? = null,

    // STACKABLE
    var stackable: Boolean = false, // Boolean DEFAULT: 0 false, 1 true
    var countObj: Array<Int?> = arrayOfNulls(10),
    var countCo: Array<Int?> = arrayOfNulls(10),

    // NOTE / LENDING
    var certlink: Int = -1,
    var certtemplate: Int = -1,
    var lentLink: Int = -1,
    var lentTemplate: Int = -1,

    // INTERFACE SPRITE
    var zoom2D: Int = 2000,
    var xAngle2D: Int = 0,
    var yAngle2D: Int = 0,
    var zAngle2D: Int = 0,
    var xOffset2D: Int = 0,
    var yOffset2D: Int = 0,
    var resizeX: Int = 128,
    var resizeY: Int = 128,
    var resizeZ: Int = 128,

    // MESHES
    var model: Int = -1,
    // MEQ
    var maleEquip1: Int = -1,                   /* CONSTRAINT! MAX 65535 */     // OP 23
    var maleEquip2: Int = -1,                   /* CONSTRAINT! MAX 65535 */     // OP 24
    var maleEquip3: Int = -1,                   /* CONSTRAINT! MAX 65535 */     // OP 78
    var primaryMaleDialogueHead: Int = -1,      /* CONSTRAINT! MAX 65535 */     // OP 90
    var secondaryMaleDialogueHead: Int = -1,    /* CONSTRAINT! MAX 65535 */     // OP 92
    // MEQ - OFFSETS
    var maleWieldX: Int = 0,
    var maleWieldZ: Int = 0,
    var maleWieldY: Int = 0,
    // FEQ
    var femaleEquip1: Int = -1,                 /* CONSTRAINT! MAX 65535 */     // OP 25
    var femaleEquip2: Int = -1,                 /* CONSTRAINT! MAX 65535 */     // OP 26
    var femaleEquip3: Int = -1,                 /* CONSTRAINT! MAX 65535 */     // OP 79
    var primaryFemaleDialogueHead: Int = -1,    /* CONSTRAINT! MAX 65535 */     // OP 91
    var secondaryFemaleDialogueHead: Int = -1,  /* CONSTRAINT! MAX 65535 */     // OP 93
    // FEQ - OFFSETS
    var femaleWieldX: Int = 0,
    var femaleWieldZ: Int = 0,
    var femaleWieldY: Int = 0,

    // COLOR SETTINGS
    var ambient: Int = 0,
    var contrast: Int = 0,
    var reColor: LinkedHashMap<Int, Int>? = null,
    var reTexture: LinkedHashMap<Int, Int>? = null,
    var recolourPalette: ByteArray? = null,

    // IMPLEMENTED IN LATER REVISIONS
    var primaryCursorOpcode: Int = -1,
    var primaryCursor: Int = -1,
    var secondaryCursorOpcode: Int = -1,
    var secondaryCursor: Int = -1,
    var primaryInterfaceCursorOpcode: Int = -1,
    var primaryInterfaceCursor: Int = -1,
    var secondaryInterfaceCursorOpcode: Int = -1,
    var secondaryInterfaceCursor: Int = -1,


) : Definition {

    override fun archiveId(): Int {
        return id ushr 8
    }

    override fun fileId(): Int {
        return id and 0xFF
    }

    private fun appendName(): String {
        if (!this.name.contentEquals(other = "null", ignoreCase = true) && this.certlink != -1) {
            ObjectUtil.nameOverride[this.certlink] = "${this.name}_noted"
            ObjectUtil.nameOverride[this.id] = this.name
        } else if (!this.name.contentEquals(other = "null", ignoreCase = true) && this.lentLink != -1) {
            ObjectUtil.nameOverride[this.lentLink] = "${this.name}_lent"
            ObjectUtil.nameOverride[this.id] = this.name
        } else if (!this.name.contentEquals(other = "null", ignoreCase = true)) {
            ObjectUtil.nameOverride[this.id] = this.name
        }

        return ObjectUtil.nameOverride.getOrDefault(key = this.id, defaultValue = "null")
    }

    override fun tomlFormat(): LinkedHashMap<String, Any> {
        // Start counter at 1 because RS does the same for their defs
        var counter = 1
        val nonDefaultsMap: LinkedHashMap<String, Any> = LinkedHashMap()

        if (!this.name.contentEquals(other = "null", ignoreCase = true)) nonDefaultsMap["name"] = appendName()
        if (this.cost != 1) nonDefaultsMap["cost"] = this.cost
        if (this.members) nonDefaultsMap["members"] = this.members
        if (this.stockMarket) nonDefaultsMap["stockmarket"] = this.stockMarket
        if (this.team != 0) nonDefaultsMap["team"] = this.team
        if (this.dummyItem != 0) nonDefaultsMap["dummyitem"] = this.dummyItem
        this.ops.forEach { operation ->
            if (!operation.isNullOrBlank()) {
                if (!operation.contentEquals(other = "Take", ignoreCase = true)) {
                    nonDefaultsMap["op$counter"] = operation
                }
            }
            counter++
        }
        counter = 1
        this.iops.forEach { iOperation ->
            if (!iOperation.isNullOrBlank()) {
                if (!iOperation.contentEquals(other = "Drop", ignoreCase = true)) {
                    nonDefaultsMap["iop$counter"] = iOperation
                }
            }
            counter++
        }

        // Param unsupported atm
        if (this.params != null) nonDefaultsMap["param"] = this.params!!

        if (this.stackable) nonDefaultsMap["stackable"] = this.stackable

        counter = 1
        this.countCo.forEach {
            if (it != null) {
                nonDefaultsMap["count$counter"] = "${countObj[counter - 1]},$it"
            }
            counter++
        }

        if (this.certlink != -1) nonDefaultsMap["certlink"] = this.certlink
        if (this.certtemplate != -1) nonDefaultsMap["certtemplate"] = this.certtemplate
        if (this.lentLink != -1) nonDefaultsMap["lentlink"] = this.lentLink
        if (this.lentTemplate != -1) nonDefaultsMap["lenttemplate"] = this.lentTemplate

        if (this.zoom2D != 2000) nonDefaultsMap["2dzoom"] = this.zoom2D
        if (this.xOffset2D != 0) nonDefaultsMap["2dxof"] = this.xOffset2D
        if (this.yOffset2D != 0) nonDefaultsMap["2dyof"] = this.yOffset2D
        if (this.xAngle2D != 0) nonDefaultsMap["2dxan"] = this.xAngle2D
        if (this.yAngle2D != 0) nonDefaultsMap["2dyan"] = this.yAngle2D
        if (this.zAngle2D != 0) nonDefaultsMap["2dzan"] = this.zAngle2D
        if (this.resizeX != 128) nonDefaultsMap["resizex"] = this.resizeX
        if (this.resizeY != 128) nonDefaultsMap["resizey"] = this.resizeY
        if (this.resizeZ != 128) nonDefaultsMap["resizez"] = this.resizeZ

        if (this.model != -1) nonDefaultsMap["model"] = this.model
        if (this.maleEquip1 != -1) nonDefaultsMap["meq1"] = this.maleEquip1
        if (this.maleEquip2 != -1) nonDefaultsMap["meq2"] = this.maleEquip2
        if (this.maleEquip3 != -1) nonDefaultsMap["meq3"] = this.maleEquip3
        if (this.primaryMaleDialogueHead != -1) nonDefaultsMap["mch1"] = this.primaryMaleDialogueHead
        if (this.secondaryMaleDialogueHead != -1) nonDefaultsMap["mch2"] = this.secondaryMaleDialogueHead
        if (this.maleWieldX != 0 || this.maleWieldY != 0 || this.maleWieldZ != 0) {
            nonDefaultsMap["manoff"] = "${this.maleWieldX},${this.maleWieldY},${this.maleWieldZ}"
        }
        if (this.femaleEquip1 != -1) nonDefaultsMap["feq1"] = this.femaleEquip1
        if (this.femaleEquip2 != -1) nonDefaultsMap["feq2"] = this.femaleEquip2
        if (this.femaleEquip3 != -1) nonDefaultsMap["feq3"] = this.femaleEquip3
        if (this.primaryFemaleDialogueHead != -1) nonDefaultsMap["fch1"] = this.primaryFemaleDialogueHead
        if (this.secondaryFemaleDialogueHead != -1) nonDefaultsMap["fch2"] = this.secondaryFemaleDialogueHead
        if (this.femaleWieldX != 0 || this.femaleWieldY != 0 || this.femaleWieldZ != 0) {
            nonDefaultsMap["femoff"] = "${this.femaleWieldX},${this.femaleWieldY},${this.femaleWieldZ}"
        }

        if (this.ambient != 0) nonDefaultsMap["ambient"] = this.ambient
        if (this.contrast != 0) nonDefaultsMap["contrast"] = this.contrast

        if (this.reColor != null) nonDefaultsMap["recolor"] = this.reColor!!
        if (this.reTexture != null) nonDefaultsMap["retexture"] = this.reTexture!!

        if (recolourPalette != null) {
            nonDefaultsMap["palette"] = this.recolourPalette!!
        }

        return nonDefaultsMap
    }

    override fun toString(): String {
        return "ObjectDefinition(id=$id, name='$name')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ObjectDefinition) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (cost != other.cost) return false
        if (members != other.members) return false
        if (stockMarket != other.stockMarket) return false
        if (team != other.team) return false
        if (dummyItem != other.dummyItem) return false
        if (!ops.contentEquals(other.ops)) return false
        if (!iops.contentEquals(other.iops)) return false
        if (params != other.params) return false
        if (stackable != other.stackable) return false
        if (!countObj.contentEquals(other.countObj)) return false
        if (!countCo.contentEquals(other.countCo)) return false
        if (certlink != other.certlink) return false
        if (certtemplate != other.certtemplate) return false
        if (lentLink != other.lentLink) return false
        if (lentTemplate != other.lentTemplate) return false
        if (zoom2D != other.zoom2D) return false
        if (xAngle2D != other.xAngle2D) return false
        if (yAngle2D != other.yAngle2D) return false
        if (zAngle2D != other.zAngle2D) return false
        if (xOffset2D != other.xOffset2D) return false
        if (yOffset2D != other.yOffset2D) return false
        if (resizeX != other.resizeX) return false
        if (resizeY != other.resizeY) return false
        if (resizeZ != other.resizeZ) return false
        if (model != other.model) return false
        if (maleEquip1 != other.maleEquip1) return false
        if (maleEquip2 != other.maleEquip2) return false
        if (maleEquip3 != other.maleEquip3) return false
        if (primaryMaleDialogueHead != other.primaryMaleDialogueHead) return false
        if (secondaryMaleDialogueHead != other.secondaryMaleDialogueHead) return false
        if (maleWieldX != other.maleWieldX) return false
        if (maleWieldZ != other.maleWieldZ) return false
        if (maleWieldY != other.maleWieldY) return false
        if (femaleEquip1 != other.femaleEquip1) return false
        if (femaleEquip2 != other.femaleEquip2) return false
        if (femaleEquip3 != other.femaleEquip3) return false
        if (primaryFemaleDialogueHead != other.primaryFemaleDialogueHead) return false
        if (secondaryFemaleDialogueHead != other.secondaryFemaleDialogueHead) return false
        if (femaleWieldX != other.femaleWieldX) return false
        if (femaleWieldZ != other.femaleWieldZ) return false
        if (femaleWieldY != other.femaleWieldY) return false
        if (ambient != other.ambient) return false
        if (contrast != other.contrast) return false
        if (reColor != other.reColor) return false
        if (reTexture != other.reTexture) return false
        if (recolourPalette != null) {
            if (other.recolourPalette == null) return false
            if (!recolourPalette.contentEquals(other.recolourPalette)) return false
        } else if (other.recolourPalette != null) return false
        if (primaryCursorOpcode != other.primaryCursorOpcode) return false
        if (primaryCursor != other.primaryCursor) return false
        if (secondaryCursorOpcode != other.secondaryCursorOpcode) return false
        if (secondaryCursor != other.secondaryCursor) return false
        if (primaryInterfaceCursorOpcode != other.primaryInterfaceCursorOpcode) return false
        if (primaryInterfaceCursor != other.primaryInterfaceCursor) return false
        if (secondaryInterfaceCursorOpcode != other.secondaryInterfaceCursorOpcode) return false
        if (secondaryInterfaceCursor != other.secondaryInterfaceCursor) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + cost.hashCode()
        result = 31 * result + members.hashCode()
        result = 31 * result + stockMarket.hashCode()
        result = 31 * result + team.hashCode()
        result = 31 * result + dummyItem.hashCode()
        result = 31 * result + ops.contentHashCode()
        result = 31 * result + iops.contentHashCode()
        result = 31 * result + (params?.hashCode() ?: 0)
        result = 31 * result + stackable.hashCode()
        result = 31 * result + countObj.contentHashCode()
        result = 31 * result + countCo.contentHashCode()
        result = 31 * result + certlink.hashCode()
        result = 31 * result + certtemplate.hashCode()
        result = 31 * result + lentLink.hashCode()
        result = 31 * result + lentTemplate.hashCode()
        result = 31 * result + zoom2D.hashCode()
        result = 31 * result + xAngle2D.hashCode()
        result = 31 * result + yAngle2D.hashCode()
        result = 31 * result + zAngle2D.hashCode()
        result = 31 * result + xOffset2D.hashCode()
        result = 31 * result + yOffset2D.hashCode()
        result = 31 * result + resizeX.hashCode()
        result = 31 * result + resizeY.hashCode()
        result = 31 * result + resizeZ.hashCode()
        result = 31 * result + model.hashCode()
        result = 31 * result + maleEquip1.hashCode()
        result = 31 * result + maleEquip2.hashCode()
        result = 31 * result + maleEquip3.hashCode()
        result = 31 * result + primaryMaleDialogueHead.hashCode()
        result = 31 * result + secondaryMaleDialogueHead.hashCode()
        result = 31 * result + maleWieldX.hashCode()
        result = 31 * result + maleWieldZ.hashCode()
        result = 31 * result + maleWieldY.hashCode()
        result = 31 * result + femaleEquip1.hashCode()
        result = 31 * result + femaleEquip2.hashCode()
        result = 31 * result + femaleEquip3.hashCode()
        result = 31 * result + primaryFemaleDialogueHead.hashCode()
        result = 31 * result + secondaryFemaleDialogueHead.hashCode()
        result = 31 * result + femaleWieldX.hashCode()
        result = 31 * result + femaleWieldZ.hashCode()
        result = 31 * result + femaleWieldY.hashCode()
        result = 31 * result + ambient.hashCode()
        result = 31 * result + contrast.hashCode()
        result = 31 * result + (reColor?.hashCode() ?: 0)
        result = 31 * result + (reTexture?.hashCode() ?: 0)
        result = 31 * result + (recolourPalette?.contentHashCode() ?: 0)
        result = 31 * result + primaryCursorOpcode.hashCode()
        result = 31 * result + primaryCursor.hashCode()
        result = 31 * result + secondaryCursorOpcode.hashCode()
        result = 31 * result + secondaryCursor.hashCode()
        result = 31 * result + primaryInterfaceCursorOpcode.hashCode()
        result = 31 * result + primaryInterfaceCursor.hashCode()
        result = 31 * result + secondaryInterfaceCursorOpcode.hashCode()
        result = 31 * result + secondaryInterfaceCursor.hashCode()
        return result
    }
}
