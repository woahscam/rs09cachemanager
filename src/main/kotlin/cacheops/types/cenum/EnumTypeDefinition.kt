package cacheops.types.cenum

import cacheops.Definition

data class EnumTypeDefinition(

    override var id: Int = -1,
    var keyType: Int? = null,
    var returnType: Int? = null,
    var defaultString: String? = null,
    var defaultInt: Int? = null,
    var table: LinkedHashMap<Long, Any>? = null
    ): Definition {

    override fun archiveId(): Int {
        return id ushr 8
    }

    override fun fileId(): Int {
        return id and 0xFF
    }
    override fun tomlFormat(): LinkedHashMap<String, Any> {
        val nonDefaultsMap: LinkedHashMap<String, Any> = LinkedHashMap()

        if (this.keyType != null) nonDefaultsMap["keytype"] = this.keyType!!
        if (this.returnType != null) {
            nonDefaultsMap["returntype"] = (EnumReturnType from this.returnType!!)?.name ?: this.returnType!!
        }
        if (!this.defaultString.isNullOrEmpty() && !this.defaultString.contentEquals("NULL")) nonDefaultsMap["dstring"] = this.defaultString!!
        if (this.defaultInt != null) nonDefaultsMap["dint"] = this.defaultInt!!
        if (this.table != null) nonDefaultsMap["params"] = this.table!!

        return nonDefaultsMap
    }
}
