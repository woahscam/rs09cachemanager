package cacheops.types.cenum

enum class EnumReturnType(val id: Int) {
    STRING(115);

    companion object {
        infix fun from(value: Int): EnumReturnType? = EnumReturnType.entries.firstOrNull { it.id == value }
    }
}