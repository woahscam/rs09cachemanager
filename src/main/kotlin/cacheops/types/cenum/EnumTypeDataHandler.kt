package cacheops.types.cenum

import cacheops.types.DataHandler
import com.displee.io.impl.InputBuffer
import com.displee.io.impl.OutputBuffer
import com.moandjiezana.toml.Toml
import java.io.File

class EnumTypeDataHandler: DataHandler<EnumTypeDefinition> {

    override fun decode(def: EnumTypeDefinition, data: ByteArray): EnumTypeDefinition {
        if (data.size > 1) {
            val buffer = InputBuffer(data)
            while (true) {
                val opcode: Int = buffer.readUnsignedByte()
                if (opcode == 0) {
                    return def
                }
                var count: Int
                when (opcode) {
                    1 -> def.keyType = buffer.readUnsignedByte()
                    2 -> def.returnType = buffer.readUnsignedByte()
                    3 -> def.defaultString = buffer.readString()
                    4 -> def.defaultInt = buffer.readInt()
                    5, 6 -> {
                        count = buffer.readUnsignedShort()
                        val table: LinkedHashMap<Long, Any> = LinkedHashMap(count)
                        for (n in 0..<count) {
                            val key = buffer.readInt()
                            table[key.toLong()] = if (opcode == 5) buffer.readString() else buffer.readInt()
                        }
                        def.table = table
                    }
                }
            }
        } else {
            return def
        }
    }

    override fun encode(def: EnumTypeDefinition): ByteArray {
        val size = determineBufferSize(def)
        val buffer = OutputBuffer(size)

        if (def.keyType != null) {
            buffer.writeByte(1)                     // Opcode
            buffer.writeByte(def.keyType!!)                 // Value
        }
        if (def.returnType != null) {
            buffer.writeByte(2)                     // Opcode
            buffer.writeByte(def.returnType!!)              // Value
        }
        if (def.defaultString != null) {
            buffer.writeByte(3)                     // Opcode
            buffer.writeString(def.defaultString!!)         // Value
        }
        if (def.defaultInt != null) {
            buffer.writeByte(4)                     // Opcode
            buffer.writeInt(def.defaultInt!!)               // Value
        }
        if (!def.table.isNullOrEmpty()) {
            if (def.returnType == 115) {
                buffer.writeByte(5)                 // Opcode
            } else {
                buffer.writeByte(6)                 // Opcode
            }
            buffer.writeShort(def.table!!.size)             // Table Size

            def.table!!.forEach {
                buffer.writeInt(it.key.toString().toInt())      // Key
                if (def.returnType == 115) {
                    buffer.writeString(it.value.toString())     // Value
                } else {
                    buffer.writeInt(it.value.toString().toInt())// Value
                }
            }
        }
        buffer.writeByte(0)                         // Terminating Opcode
        return buffer.raw()
    }

    override fun determineBufferSize(def: EnumTypeDefinition): Int {
        var bufSize = 0

        if (def.keyType != null) {
            bufSize += 2
        }
        if (def.returnType != null) {
            bufSize += 2
        }
        if (def.defaultString != null) {
            bufSize++
            bufSize += (def.defaultString!!.toByteArray().size + 1)
        }
        if (def.defaultInt != null) {
            bufSize += 5
        }
        if (def.table != null) {
            bufSize += 3

            def.table!!.forEach { entries ->
                if (def.returnType == 115) {
                    bufSize += 4
                    bufSize += ((entries.value as String).toByteArray().size + 1)
                } else {
                    bufSize += 8
                }
            }
        }
        bufSize++
        return bufSize
    }

    override fun tomlToDefinition(path: String): HashMap<Int, EnumTypeDefinition> {
        val encodeMap: HashMap<Int, EnumTypeDefinition> = HashMap()
        val file = File(path)
        file.listFiles()?.forEach { defFile ->
            val def = decodeTOML(defFile)
            if (def != null) {
                encodeMap[def.id] = def
            }
        }
        return encodeMap
    }

    override fun decodeTOML(file: File): EnumTypeDefinition? {
        val idString = file.name.filter { it.isDigit() }
        val tableName = file.nameWithoutExtension.removePrefix("ENUM").filter { !it.isDigit() }.removePrefix("_").lowercase()

        val toml: Toml? = Toml().read(file)

        if (toml != null) {
            val table = toml.getTable(tableName)
            val keyType: Long? = table.getLong("keytype")
            val keyT: Int? = keyType?.toInt()

            var returnType: Any = try {
                table.getLong("returntype").toInt()
            } catch (ex: ClassCastException) {
                table.getString("returntype")
            }
            if (returnType is String) {
                if (returnType.contentEquals("STRING", true)) {
                    returnType = 115
                }
            }
            val ret = returnType.toString().toInt()

            val dstring: String? = table.getString("dstring")
            val dint: Long? = table.getLong("dint")
            val din: Int? = dint?.toInt()


            val params: Toml? = toml.getTable("params")
            val map: LinkedHashMap<String, Any>? = params?.toMap()
            var linkedMap: LinkedHashMap<Long, Any>? = null
            if (map != null) {
                linkedMap = LinkedHashMap()
                map.forEach {
                    linkedMap[it.key.toInt().toLong()] = it.value
                }
            }
            return EnumTypeDefinition(id = idString.toInt(), keyType = keyT, returnType = ret, defaultString = dstring, defaultInt = din, linkedMap)
        } else {
            System.err.println("[NULL DEFINITION FROM FILE ALERT] !!! ${idString} is null! ${file.name}")
            return null
        }
    }
}