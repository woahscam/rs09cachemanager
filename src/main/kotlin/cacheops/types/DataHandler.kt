package cacheops.types

import cacheops.Definition
import java.io.File

interface DataHandler<Definition> {

    fun decode(def: Definition, data: ByteArray): Definition

    fun encode(def: Definition): ByteArray

    fun determineBufferSize(def: Definition): Int

    fun tomlToDefinition(path: String): HashMap<Int, Definition>

    fun decodeTOML(file: File): Definition?
}