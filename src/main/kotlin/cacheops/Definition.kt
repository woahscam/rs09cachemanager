package cacheops

interface Definition {
    var id: Int

    fun tomlFormat(): LinkedHashMap<String, Any>

    fun archiveId(): Int

    fun fileId(): Int
}