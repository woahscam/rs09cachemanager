import logger.LogLevel

object Settings {

    const val REVISION = 530
    const val LOG_STARTUP_CACHE_INFO = true
    val LOG_LEVEL = LogLevel.VERBOSE

}