package logger

import Settings
import cacheops.Cache
import cacheops.util.Indices
import com.github.ajalt.colormath.model.Ansi256
import com.github.ajalt.mordant.rendering.*
import com.github.ajalt.mordant.rendering.BorderType.Companion.SQUARE_DOUBLE_SECTION_SEPARATOR
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import com.github.ajalt.mordant.terminal.Terminal
import com.github.ajalt.mordant.widgets.HorizontalRule
import java.text.SimpleDateFormat
import java.util.*

/**
 * Handles server log printing
 * @author Ceikry
 * Thanks to the awesome library made by AJ Alt
 */
object SystemLogger {
    val t = Terminal()
    val errT = t.forStdErr()
    val formatter = SimpleDateFormat("HH:mm:ss")

    private fun getTime(): String {
        return "[" + formatter.format(Date(System.currentTimeMillis())) + "]"
    }

    @JvmStatic
    fun processLogEntry(clazz: Class<*>, log: Log, message: String) {
        when (log) {
            Log.DEBUG -> {
                val msg = TextColors.cyan("${getTime()}: [${clazz.simpleName}] $message")
                t.println(msg)
            }

            Log.FINE -> {
                if (Settings.LOG_LEVEL < LogLevel.VERBOSE)
                    return
                val msg = TextColors.gray("${getTime()}: [${clazz.simpleName}] $message")
                t.println(msg)
            }

            Log.INFO -> {
                if (Settings.LOG_LEVEL < LogLevel.DETAILED)
                    return

                val msg = "${getTime()}: [${clazz.simpleName}] $message"
                t.println(msg)
            }

            Log.WARN -> {
                if (Settings.LOG_LEVEL < LogLevel.CAUTIOUS)
                    return

                val msg = TextColors.yellow("${getTime()}: [${clazz.simpleName}] $message")
                t.println(msg)
            }

            Log.ERR -> {
                val msg = "${getTime()}: [${clazz.simpleName}] $message"
                errT.println(msg)
            }
        }
    }

    fun logCache(message: String) {
        log(this::class.java, Log.FINE, "[CACHE] $message")
    }

    fun logCacheInformation(path: String) {
        t.println(
            table {
                borderType = SQUARE_DOUBLE_SECTION_SEPARATOR
                outerBorder = true
                captionTop(TextStyle(Ansi256(105))("Cache loaded from: ${(TextStyle(Ansi256(250))(path))}"))
                column(0) {
                    //align = TextAlign.LEFT
                    borders = Borders.ALL
                }
                column(1) {
                    //align = TextAlign.LEFT
                    borders = Borders.ALL
                }
                column(2) {
                    //align = TextAlign.LEFT
                    borders = Borders.ALL
                }
                column(3) {
                    //align = TextAlign.LEFT
                    borders = Borders.ALL
                }
                column(4) {
                    //align = TextAlign.LEFT
                    borders = Borders.ALL
                }
                column(5) {
                    borders = Borders.ALL
                }
                header {
                    style(Ansi256(105), bold = true)
                    row("${Settings.REVISION} Cache", "CRC", "# Archive", "# File")
                }
                body {
                    borders = Borders.TOM_BOTTOM
                    Cache.library.indices().forEach { Index ->
                        var fileSize = 0
                        Index.archives().forEach { Archive ->
                            fileSize += Archive.files().size
                        }
                        row("[${Indices.entries[Index.id].index}] ${Indices.entries[Index.id].indexName}", Index.crc, Index.archiveIds().size, fileSize)
                    }

                }
            },
            align = TextAlign.CENTER
        )

        t.println(HorizontalRule())
    }
}

/**
 * Logs a message to the server console
 * @param origin simply put (Kotlin) this::class.java or (Java) this.getClass()
 * @param type the type of log: Log.FINE (default, visible on VERBOSE), Log.INFO (visible on DETAILED), Log.WARN (visible on CAUTIOUS), Log.ERR (always visible)
 * @param message the actual message to log.
 */
fun log(origin: Class<*>, type: Log, message: String) {
    SystemLogger.processLogEntry(origin, type, message)
}

enum class LogLevel {
    SILENT,
    CAUTIOUS,
    DETAILED,
    VERBOSE
}

enum class Log {
    FINE,
    INFO,
    WARN,
    ERR,
    DEBUG
}
