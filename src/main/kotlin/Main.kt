import cacheops.Cache
import cacheops.types.obj.ObjectDefinition
import cacheops.types.obj.ObjectUtil
import logger.SystemLogger
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.size < 1) {
        println("Use: Cache/location/path mode")
        println("Mode: extract / pack")
        exitProcess(0)
    }

    val cachePath = args[0]
    //val processType = args[1]

    if (!Cache.init(cachePath)) {
        println("Could not find cache at location: ${args[0]}")
        exitProcess(0)
    }

    if (Settings.LOG_STARTUP_CACHE_INFO) {
        SystemLogger.logCacheInformation(cachePath)
    }

//    when (processType.lowercase()) {
//        "extract" -> println("We do a lil extractin")
//        "pack" -> println("We do a lil packin")
//        else -> {
//            println("Process mode invalid, please try again.")
//            println("Mode: extract / pack")
//            exitProcess(0)
//        }
//    }

//    Cache.CONFIGURATION_ENUM.loopDefToTOML()
    //Cache.CONFIGURATION_ENUM.packLooseFromDir()
    //Cache.CONFIGURATION_OBJECT.loopDefToTOML()
}